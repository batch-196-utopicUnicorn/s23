console.log("Hello World");

//JS Objects
//JS Objects is a way to define a real world object with its own characteristics.
//It is also a way to organize data with context through the use of key-value pairs.


//Describes something with characteristics, use object
let dog = {

	breed: "Husky",
	color: "White",
	legs: 4,
	isHuggable: false,
	isLoyal: true

}

let myFavoriteGame ={

	title: "Team Fight Tactics",
	publisher: "Riot Games",
	year: 2019,
	price: 0,
	isAvailable: true
}

console.log(myFavoriteGame)

//If [] are used to create arrays, what did you use to create your object?
//[] - array literals create arrays
//{} - object literals create objects

//Objects are composed of key-value pairs. Key provide label to your values.
//Key: value
//Each key-value pair together are called properties

//Accessing array items = arrName[index]
//Accessing Object properties = objectName.propertyName

console.log(myFavoriteGame.title);

//what if I want to access the publisher of our videGame?
console.log(myFavoriteGame.publisher);

//Can we also update the properties of our object?
myFavoriteGame.price = "Free to play"
console.log(myFavoriteGame.price);

myFavoriteGame.title = "Final Fantasy X";
myFavoriteGame.publisher = "Square Enix";
myFavoriteGame.year = 2001;

console.log(myFavoriteGame);

//Objects can not only have primitive values like strings, numbers or boolean
//It can also contain objects and arrays.

let course = {

	title: "Philisophy 101",
	description: "Learn the values of life",
	price: 5000,
	isActive: true,
	instructors: ["Mr. Johnson", "Mrs. Smith", "Mr. Francis"]

};
console.log(course);
//Can we access the course's instructors array?
console.log(course.instructors);
//How can we access, Mrs. Smith, an instructor from our instructors array?
console.log(course.instructors[1]);
//Can we use the instructor array's array methods?
//How can we delete Mr. Francis from our instructors array?
course.instructors.pop();
console.log(course);
//objectName.propertyNameArr.method();


course.instructors.push("Mr. McGee");
console.log(course.instructors);

let isAnInstructor = course.instructors.includes("Mr. Johnson");
console.log(isAnInstructor);

//Create a function to be able to add new instructors to our object


function addNewInstructor(newInstructor){
	if(course.instructors.includes(newInstructor)){
		console.log("Instructor already added")
	}else{


	course.instructors.push(newInstructor);
	console.log(`Added Instructor ${newInstructor}`)
	}
}

addNewInstructor("Mr. Marco");
addNewInstructor("Mr. Smith");


//We can also create/initialize an empty object and then add its properties afterwards

let instructor = {};


instructor.name = "James Johnson";

instructor.occupation = "Instructor";
instructor.age = 56;
instructor.gender = "male";
instructor.department = "Humanities";
instructor.salary = 50000;
instructor.teaches = ["Philisophy", "Humanities", "Logic"]
console.log(instructor);


instructor.address = {
	street: "#1 Maginhawa St.",
	city: "Quezon City",
	country: "Philippines"
}
console.log(instructor);
//How will we access the street property of our instructor's address?
console.log(instructor.address.street);

//create objects using a constructor function

//create a reusable function to create objects whose structure and keys are the same. Think of creating a function that serves a blueprint for an object.

function Superhero(name, superpower, powerLevel){


	//"this" keyword when added in a constructor function refers to the object that will be made by the function.
	/*
		{
			name: <valueOfParameterName>
			superpower: <valueOfParametersuperpower>
			powerLevel: <valueOfParameterPowerLevel
		}
	*/
	this.name = name;
	this.superpower = superpower;
	this.powerLevel = powerLevel;
};


//Create an object out of our Superhero constructor function
//new Keyworld is added to allow us to create a new object out of our function
let superhero1 = new Superhero("Saitama", "One Punch", 30000);
console.log(superhero1);



function Product(name, brand, price){

	this.name = name;
	this.brand = brand;
	this.price = price;
}

let laptop1 = new Product("Aspire 3000", "Dell", 123456);
let laptop2 = new Product("Lenovo Magic", "Lenovo", 987456);

console.log(laptop1);
console.log(laptop2);


//Object Methods
//Object methods are functions that are associated with an object.
//A function is a property of an object and that function belongs to the object.
//Methods are task that an object can perform or do.


//arrayName.method()

let person = {
	name: "Slim Shady",
	talk: function(){

		//methods are functions associated as a property of an object.
		//they are anonymous functions we can invoke using the property of the object.
		//"this" refers to the object where the method is associated.
		console.log(this);
		console.log("Hi! My name is, what? My name is who? " + this.name);

	}
}

let person2 = {
	name: "Dr. Dre",
	greet: function(friend){

		//greet() should be able to receive a single object
		console.log("Good day, " + friend.name);
	}
}
person.talk();

person2.greet(person);


//Create a constructor with a built-in method

function Dog(name, breed){

	/*
		name: <valueParameterName>
		breed: <valueParameterBreed>
	*/

	this.name = name;
	this.breed = breed;
	this.greet = function(friend){
		console.log("Bark! bark, " + friend.name);
	}
}

let dog1 = new Dog("King", "Labrador");
console.log(dog1);

dog1.greet(person);
dog1.greet(person2);

let dog2 = new Dog("Storm", "Labrador");
console.log(dog2)