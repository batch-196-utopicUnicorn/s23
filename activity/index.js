

let trainer = {
	name: "Ash Ketchum",
	age: 14,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		Kanto: ["Misty", "Brock"],
		Hoen: ["May", "Max"]
	},
	talk: function(){
		console.log("Pikachu! I choose you")
	}
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:");
trainer.talk();

function Pokemon(name, level){

	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level * 1.5;
	this.tackle = function(otherPokemon){
		console.log(`${this.name}'s tackled ${otherPokemon.name}`)
		console.log(`${otherPokemon.name}'s health is now reduced to ${otherPokemon.health - this.attack}`);
		let currentHealth = otherPokemon.health - this.attack
		if(currentHealth <= 0){
			console.log(otherPokemon.faint())
		}else{
			console.log("Battle continue")
		}
	}
	this.faint = function(){
		console.log(this.name +  " now has 0 health")
		console.log(this.name + " has fainted")

	}
}

pokemon1 = new Pokemon("Onyx", 18);
console.log(pokemon1);

pokemon2 = new Pokemon("Dragonite", 83);
console.log(pokemon2);

pokemon3 = new Pokemon("Magicarp", 19);
console.log(pokemon3)

pokemon1.tackle(pokemon2);
pokemon2.tackle(pokemon1);